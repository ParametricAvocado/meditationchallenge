﻿using UnityEngine;
using System.Collections;

public class SmokeThread : MonoBehaviour
{
    [SerializeField]
    LineRenderer lineRenderer;
    [SerializeField]
    float amplitude = 0.5f;
    [SerializeField]
    float frequency = 0.1f;
    [SerializeField]
    float segmentLength = 0.5f;
    [SerializeField]
    float subSway = 2;
    [SerializeField]
    float subSwayFreq = 1;
    [SerializeField]
    float swaySpeed = 2;
    [SerializeField]
    int resolution = 8;

    Vector3[] positions;
    void Awake()
    {
        positions = new Vector3[resolution];
    }

    float Sway(float segment)
    {
        return (Mathf.Sin(Time.time* swaySpeed + segment * subSwayFreq * frequency)* subSway + Mathf.Sin(Time.time* swaySpeed + segment * frequency)) *segment * amplitude;
    }

    void Update()
    {
        positions = new Vector3[resolution];
        for (int p = 0; p < positions.Length; p++)
        {
            positions[p] = new Vector3(Sway(p), p * segmentLength, 0);
        }

        lineRenderer.SetPositions(positions);
    }

    void OnValidate()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }
}
