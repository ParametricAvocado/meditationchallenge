﻿using UnityEngine;
using UnityEngine.Events;

public abstract class GameController : MonoBehaviour
{
    private static bool isPlaying;
    private static bool isPaused;
    
    public static UnityEvent onPaused = new UnityEvent();
    public static UnityEvent onUnpaused = new UnityEvent();

    public static UnityEvent onGameBegin = new UnityEvent();
    public static UnityEvent onGameEnd = new UnityEvent();
    public static UnityEvent onGameReset = new UnityEvent();

    public static UnityEvent onWin = new UnityEvent();
    public static UnityEvent onLose = new UnityEvent();

    public static bool IsPlaying { get { return isPlaying; } }

    public static bool IsPaused { get { return isPaused; } }

    public static void OnGameBegin()
    {
        if (!isPlaying) { onGameBegin.Invoke(); isPlaying = true; }
    }

    public static void OnGameEnd()
    {
        if (isPlaying) { onGameEnd.Invoke(); isPlaying = false; }
    }
    public static void OnGameReset()
    {
        onGameReset.Invoke();
    }
    public static void Pause()
    {
        if (!isPaused) { onPaused.Invoke(); isPaused = true; }
    }

    public static void Unpause()
    {
        if (isPaused) { onUnpaused.Invoke(); isPaused = false; }
    }
    public static void OnWin()
    {
        if(isPlaying) { onWin.Invoke(); OnGameEnd(); }
    }

    protected static void OnLose()
    {
        if(isPlaying) { onLose.Invoke(); OnGameEnd(); }
    }
}
