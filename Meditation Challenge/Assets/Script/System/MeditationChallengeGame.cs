﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System;
using System.Collections.Generic;

public class MeditationChallengeGame : GameController
{
    public float startingMeditationPoints;
    public float maxMeditationPoints;
    public float meditationPointsGain;
    public float meditationPointsLoss;
    public float quickReactionBonus = 20;
    public float quickReactionBonusTime = 0.5f;

    public AudioSource quickReactionSound;

    public AnimationCurve annoyanceCurve;

    private float lastAnnoyanceTime;
    private float gameBeginTime;

    private static List<RoomProp> roomProps = new List<RoomProp>();

    private static float s_meditationPoints;
    private static float s_maxMeditationPoints;
    private static float s_meditationPointsLoss;
    private static float s_quickReactionBonus;
    private static float s_quickReactionBonusTime;

    private static UnityEvent onQuickActionBonus = new UnityEvent();

    public static float MeditationPoints
    {
        get
        {
            return s_meditationPoints;
        }

        private set
        {
            s_meditationPoints = value;
            if (s_meditationPoints <= 0)
            {
                OnLose();
            }
            if (s_meditationPoints >= s_maxMeditationPoints)
            {
                OnWin();
            }
        }
    }

    public static float MaxMeditationPoints { get { return s_maxMeditationPoints; } }

    void Awake()
    {
        onGameBegin.AddListener(
            delegate
            {
                s_meditationPoints = startingMeditationPoints;
                lastAnnoyanceTime = Time.time;
                gameBeginTime = Time.time;
            });

        s_maxMeditationPoints = maxMeditationPoints;
        s_meditationPointsLoss = meditationPointsLoss;
        s_quickReactionBonusTime = quickReactionBonusTime;

        if (quickReactionSound)
            onQuickActionBonus.AddListener(quickReactionSound.Play);
    }

    void FixedUpdate()
    {
        if (IsPlaying && !IsPaused)
        {
            MeditationPoints += meditationPointsGain * Time.deltaTime;

            float annoyanceTime = annoyanceCurve.Evaluate(Time.time - gameBeginTime);
            if (Time.time > lastAnnoyanceTime + annoyanceTime)
            {
                ActivateRandomProp();
                lastAnnoyanceTime = Time.time;
            }
        }
    }
    public static void ConsumeMeditationPoints()
    {
        if (IsPlaying && !IsPaused)
        {
            MeditationPoints -= s_meditationPointsLoss * Time.deltaTime;
        }
    }
    void ActivateRandomProp()
    {
        if (roomProps.Count > 1)
        {
            int index = UnityEngine.Random.Range(1, roomProps.Count);
            roomProps[index].Activate();
            RoomProp swapProp = roomProps[0];
            roomProps[0] = roomProps[index];
            roomProps[index] = swapProp;
        }
    }

    public static void OnPropDeactivated(float timeSinceActivation)
    {
        if (timeSinceActivation < s_quickReactionBonusTime)
        {
            MeditationPoints += s_quickReactionBonus;
            onQuickActionBonus.Invoke();
        }
    }

    public static void RegisterProp(RoomProp roomProp)
    {
        roomProps.Add(roomProp);
    }
}
