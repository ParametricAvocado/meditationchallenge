﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;

public class MusicAccelerator : MonoBehaviour {

    [Range(1, 2)]
    public float speed;
    public AudioMixer mixer;

    void Update()
    {
        mixer.SetFloat("music_speed", speed);
        mixer.SetFloat("music_pitch_compensation", 1 / speed);
    }
}
