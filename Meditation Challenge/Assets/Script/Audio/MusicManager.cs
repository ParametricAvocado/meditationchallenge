﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;

public class MusicManager : MonoBehaviour
{
    public AudioMixer mixer;
    public AudioMixerSnapshot main_menu;
    public AudioMixerSnapshot normal_gameplay;
    public AudioMixerSnapshot end_game;
    public AudioSource win_sound;
    public AudioSource lose_sound;

    AudioMixerSnapshot[] MixerSnapshots
    {
        get { return new AudioMixerSnapshot[] { main_menu, normal_gameplay, end_game }; }
    }

    void Awake()
    {
        GameController.onGameBegin.AddListener(OnGameBegin);
        GameController.onGameEnd.AddListener(OnGameEnd);
        GameController.onGameReset.AddListener(OnGameReset);
        GameController.onWin.AddListener(OnWin);
        GameController.onLose.AddListener(OnLose);
    }

    void Update()
    {
        if (GameController.IsPlaying)
            mixer.SetFloat("critical_vol", Mathf.Lerp(0, -40, MeditationChallengeGame.MeditationPoints / MeditationChallengeGame.MaxMeditationPoints * 2));
        else
            mixer.SetFloat("critical_vol", -80);
    }

    void OnWin()
    {
        if (win_sound)
            win_sound.Play();
    }
    void OnLose()
    {
        if (lose_sound)
            lose_sound.Play();
    }
    void OnGameBegin()
    {
        mixer.TransitionToSnapshots(MixerSnapshots, new float[] { 0, 1, 0 }, 1.5f);
    }
    void OnGameEnd()
    {
        mixer.TransitionToSnapshots(MixerSnapshots, new float[] { 0, 0, 1 }, 2f);
    }
    void OnGameReset()
    {
        mixer.TransitionToSnapshots(MixerSnapshots, new float[] { 1, 0, 0 }, 0.5f);
    }
}
