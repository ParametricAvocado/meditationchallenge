﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class IntroSequence : MonoBehaviour {
    [SerializeField]
    Animator directorAnimator;
    [SerializeField]
    Animator boxAnimator;
    [SerializeField]
    AudioSource gruntSource;
    [SerializeField]
    AudioSource doorbellSource;

    public static UnityEvent onIntroFinished = new UnityEvent();
    public static UnityEvent onMainScreenHidden = new UnityEvent();

    void Awake()
    {
        GameController.onGameBegin.AddListener(OnGameBegin);
        GameController.onGameReset.AddListener(OnGameReset);
    }
    void OpenBox()
    {
        boxAnimator.SetTrigger("open");
    }
    void Grunt()
    {
        gruntSource.Play();
    }

    void Doorbell()
    {
        doorbellSource.Play();
    }
    void IntroFinished()
    {
        onIntroFinished.Invoke();
    }
    void MainScreenHidden()
    {
        onMainScreenHidden.Invoke();
    }
    void OnGameBegin()
    {
        directorAnimator.SetTrigger("start");
    }
    void OnGameReset()
    {
        directorAnimator.SetTrigger("reset");
    }
}
