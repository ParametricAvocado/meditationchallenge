﻿using UnityEngine;
using System.Collections;

public class UIPanel : MonoBehaviour {

    [SerializeField]
    protected UIAnimator panelAnimator;

    protected virtual void OnValidate()
    {
        panelAnimator = GetComponent<UIAnimator>();
    }
}
