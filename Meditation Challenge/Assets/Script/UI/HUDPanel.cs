﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUDPanel : UIPanel {
    [SerializeField]
    Image barPositive;
	[SerializeField]
	Image barNegative;
    void Awake()
    {
        IntroSequence.onMainScreenHidden.AddListener(panelAnimator.Show);
        GameController.onGameEnd.AddListener(panelAnimator.Hide);
    }
    void Update()
    {
		float concentration = MeditationChallengeGame.MeditationPoints / MeditationChallengeGame.MaxMeditationPoints;
		barPositive.fillAmount = (concentration -0.5f)*2;
		barNegative.fillAmount = 1-concentration*2;
    }
}
