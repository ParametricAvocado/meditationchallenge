﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(Animator))]
public class UIAnimator : MonoBehaviour
{
    [SerializeField]
    protected Animator animator;
    [SerializeField]
    protected float animationDuration;

    public void Show()
    {
        animator.SetBool("visible", true);
    }
    public void Hide()
    {
        animator.SetBool("visible", false);
    }

    void Awake()
    {
        if (animationDuration > 0)
            animator.speed = 1 / animationDuration;
    }

    void OnValidate()
    {
        animator = GetComponent<Animator>();
    }
}
