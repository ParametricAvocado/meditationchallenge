﻿using UnityEngine;
using UnityEngine.UI;

public class WinScreen : UIPanel {

    [SerializeField]
    Button restartButton;

    void Awake()
    {
        GameController.onGameReset.AddListener(panelAnimator.Hide);

        restartButton.onClick.AddListener(GameController.OnGameReset);
        Character.onFinishedWonAnimation.AddListener(panelAnimator.Show);
    }
}
    
