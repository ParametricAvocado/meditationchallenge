﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LoseScreen : UIPanel {
    [SerializeField]
    Button restartButton;
    void Awake()
    {
        GameController.onLose.AddListener(panelAnimator.Show);
        GameController.onGameReset.AddListener(panelAnimator.Hide);
        restartButton.onClick.AddListener(GameController.OnGameReset);
    }


}
