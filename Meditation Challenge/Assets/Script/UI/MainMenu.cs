﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(UIAnimator))]
public class MainMenu : UIPanel {
    [SerializeField]
    Button beginButton;


    void Awake()
    {
        beginButton.onClick.AddListener(GameController.OnGameBegin);

        GameController.onGameBegin.AddListener(panelAnimator.Hide);
		GameController.onGameReset.AddListener(panelAnimator.Show);
        IntroSequence.onIntroFinished.AddListener(panelAnimator.Show);
    }
}
