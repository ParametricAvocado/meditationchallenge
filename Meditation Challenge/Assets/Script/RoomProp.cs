﻿using UnityEngine;
[RequireComponent(typeof(Animator))]
public class RoomProp : MonoBehaviour
{
    [SerializeField]
    Animator animator;
    [SerializeField]
    BoxCollider2D coll;
    bool active;

    [SerializeField]
    AudioSource activeLoopSound;

    [SerializeField]
    AudioSource activateSound;

    [SerializeField]
    AudioSource deactivateSound;

    [SerializeField]
    AudioSource eventSound;
    float activationTime;
    void Awake()
    {
        GameController.onGameBegin.AddListener(Reset);
        MeditationChallengeGame.RegisterProp(this);
    }

    void Reset()
    {
        animator.SetTrigger("reset");
        active = false;
        coll.enabled = false;
        if (activeLoopSound)
            activeLoopSound.Stop();
    }

    public void Activate()
    {
        if (!active)
        {
            animator.SetTrigger("activate");
            active = true;
            activationTime = Time.time;
            coll.enabled = true;
            if (activateSound)
                activateSound.Play();
            if (activeLoopSound)
                activeLoopSound.Play();
        }
    }

    public void Deactivate()
    {
        if (active)
        {
            animator.SetTrigger("deactivate");
            active = false;
            coll.enabled = false;
            if (deactivateSound)
                deactivateSound.Play();
            if (activeLoopSound)
                activeLoopSound.Stop();

            MeditationChallengeGame.OnPropDeactivated(Time.time - activationTime);
        }
    }

    void FixedUpdate()
    {
        if (active)
            MeditationChallengeGame.ConsumeMeditationPoints();
    }

    void OnMouseDown()
    {
        Deactivate();
    }

    void OnValidate()
    {
        animator = GetComponent<Animator>();
        coll = GetComponent<BoxCollider2D>();
    }

    void OnAnimationEvent()
    {
        if (eventSound)
            eventSound.Play();
    }
}
