﻿using UnityEngine;
using UnityEngine.Events;

public class Character : MonoBehaviour
{
    [SerializeField]
	Animator animator;

    public static UnityEvent onFinishedWonAnimation = new UnityEvent();

    void Awake()
    {
        GameController.onWin.AddListener(OnWin);
        GameController.onGameReset.AddListener(OnReset);
        
    }

    void Update()
    {
        if (animator)
        {
            if (GameController.IsPlaying)
                animator.SetFloat("concentration", MeditationChallengeGame.MeditationPoints / MeditationChallengeGame.MaxMeditationPoints);
            else
                animator.SetFloat("concentration", 0.5f);
        }
    }

    void OnWin()
    {
        animator.SetTrigger("won");
    }

    void OnReset()
    {
        animator.SetTrigger("reset");
    }


    void OnFinishedWonAnimation()
    {
        onFinishedWonAnimation.Invoke();
    }
    void OnValidate()
    {
        animator = GetComponent<Animator>();
    }
}
